package ru.nsu.mmichurov.dis.task1;

import org.jetbrains.annotations.NotNull;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import java.io.InputStream;
import java.util.*;

public class OsmXmlParser {
    private static class QualifiedNames {
        public static final QName key = new QName("k");
        public static final QName tag = new QName("tag");
        public static final QName node = new QName("node");
        public static final QName user = new QName("user");
        public static final QName changeSet = new QName("changeset");
    }

    public static class ParseResult {
        final HashMap<String, Set<String>> userChanges = new HashMap<>();
        final HashMap<String, Integer> tagUses = new HashMap<>();

        private ParseResult() {
        }
    }

    public static ParseResult parseXml(final @NotNull InputStream input) throws XMLStreamException {
        final var inputFactory = XMLInputFactory.newInstance();
        final var reader = inputFactory.createXMLEventReader(input);
        final var parentElements = new ArrayDeque<StartElement>();

        final var result = new ParseResult();

        while (reader.hasNext()) {
            final var nextEvent = reader.nextEvent();

            if (nextEvent.isStartElement()) {
                final var element = nextEvent.asStartElement();

                if (isNode(element)) {
                    handleNode(element, result);
                }

                if (isTag(element)) {
                    handleTag(element, parentElements, result);
                }

                parentElements.addLast(element);
            }

            if (nextEvent.isEndElement()) {
                parentElements.removeLast();
            }
        }

        reader.close();

        return result;
    }

    private static void handleNode(
        final @NotNull StartElement element,
        final @NotNull ParseResult parseResult
    ) {
        final var user = element.getAttributeByName(QualifiedNames.user);
        final var changeSet = element.getAttributeByName(QualifiedNames.changeSet);

        if (null == user || null == changeSet) {
            return;
        }

        parseResult.userChanges.putIfAbsent(user.getValue(), new HashSet<>());
        parseResult.userChanges.get(user.getValue()).add(changeSet.getValue());
    }

    private static void handleTag(
        final @NotNull StartElement element,
        final @NotNull Deque<StartElement> parentElements,
        final @NotNull ParseResult parseResult
    ) {
        final var key = element.getAttributeByName(QualifiedNames.key);
        final var parent = parentElements.getLast();

        if (!isNode(parent) || null == key) {
            return;
        }

        parseResult.tagUses.putIfAbsent(key.getValue(), 0);
        parseResult.tagUses.put(key.getValue(), parseResult.tagUses.get(key.getValue()) + 1);
    }

    private static boolean isNode(final @NotNull StartElement element) {
        return element.getName().equals(QualifiedNames.node);
    }

    private static boolean isTag(final @NotNull StartElement element) {
        return element.getName().equals(QualifiedNames.tag);
    }
}
