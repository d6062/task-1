package ru.nsu.mmichurov.dis.task1;

public class Pair<T1, T2> {
    public final T1 item1;
    public final T2 item2;

    public Pair(T1 item1, T2 item2) {
        this.item1 = item1;
        this.item2 = item2;
    }

    public static <U, V> Pair<U, V> of(U item1, V item2) {
        return new Pair<>(item1, item2);
    }
}
