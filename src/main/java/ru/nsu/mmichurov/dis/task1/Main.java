package ru.nsu.mmichurov.dis.task1;

import org.apache.commons.cli.*;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.xml.stream.XMLStreamException;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Comparator;

public class Main {
    private static final String APP_NAME = "task1";

    private static final String FILE_OPTION = "file";
    private static final String HELP_OPTION = "help";

    private static final int BUFFER_SIZE = 1024 * 1024 * 10;

    private static final Logger log = LogManager.getLogger(Main.class);

    private static Options makeOptions() {
        return new Options()
            .addOption(
                Option.builder()
                    .option(FILE_OPTION)
                    .hasArg()
                    .desc("bz2 archive file name")
                    .required()
                    .build()
            )
            .addOption(HELP_OPTION, "print usage");
    }

    private static void printHelp(final @NotNull Options options) {
        final var formatter = new HelpFormatter();
        final var header = "Extract Open Street Map data and compute some statistics.";
        formatter.printHelp(APP_NAME, header, options, null);
    }

    public static void main(final @NotNull String[] args) {
        final var options = makeOptions();
        final var parser = new DefaultParser();

        try {
            final var commandLine = parser.parse(options, args);

            if (commandLine.hasOption(HELP_OPTION)) {
                printHelp(options);
                return;
            }

            run(commandLine.getOptionValue(FILE_OPTION));
        } catch (final ParseException e) {
            System.err.println(e.getMessage());
            printHelp(options);
        }
    }

    private static void run(final @NotNull String fileName) {
        try (
            final var rawInput = new FileInputStream(fileName);
            final var xmlInput = new BZip2CompressorInputStream(new BufferedInputStream(rawInput, BUFFER_SIZE))
        ) {
            final var result = OsmXmlParser.parseXml(xmlInput);
            printStatistics(result);
        } catch (IOException | XMLStreamException e) {
            System.err.println(e.getMessage());
            log.error(e);
        }
    }

    private static void printStatistics(final @NotNull OsmXmlParser.ParseResult parseResult) {
        System.out.println("User changes count:");

        parseResult.userChanges.entrySet().stream()
            .map(it -> Pair.of(it.getKey(), it.getValue().size()))
            .sorted(Comparator.<Pair<String, Integer>>comparingInt(it -> it.item2).reversed())
            .forEach(it -> System.out.println(it.item1 + " - " + it.item2));

        System.out.println();

        System.out.println("Unique tags used - " + parseResult.tagUses.size());

        System.out.println();

        System.out.println("Tagged nodes count:");

        parseResult.tagUses.forEach((tag, uses) -> System.out.println(tag + " - " + uses));
    }
}
